package me.jangulaslam.javasamples;

import java.util.Stack;


public class Others {
	public static void runTests() {
		//towerOfHanoi();
		towerOfHanoi2();
	}

	// tower of hanoi (using int to indicate poles)
	private static void towerOfHanoi(int n, int src, int spare, int dest) {
		if (n > 0) {
			towerOfHanoi(n - 1, src, dest, spare);
			System.out.format("Moving disk: %d from source pole: %d to destination pole: %d (spare pole: %d)\n", n, src, dest, spare);
			towerOfHanoi(n - 1, spare, src, dest);
		}
	}
	
	public static void towerOfHanoi() {
		towerOfHanoi(1, 1, 2, 3);
	}

	// tower of hanoi2 (using Stacks to indicate poles)
	private static void towerOfHanoi2(int n, Stack<Integer> src, Stack<Integer> dest, Stack<Integer> spare) {
		//System.out.format("\nRunning => Source pole: %s, Destination pole: %s, Spare pole: %s, n: %d\n", src, dest, spare, n);
		if (n > 0) {
			towerOfHanoi2(n - 1, src, spare, dest);
			//System.out.format("Before moving disk: %d from source pole: %s to destination pole: %s (spare pole: %s)\n", n, src, dest, spare);
			dest.push(src.pop());
			System.out.format("After moving disk: %d from source pole: %s to destination pole: %s (spare pole: %s)\n", n, src, dest, spare);
			towerOfHanoi2(n - 1, spare, dest, src);
		}
	}

	public static void towerOfHanoi2() {
		Stack<Integer> src = new Stack<>();
		Stack<Integer> dest = new Stack<>();
		Stack<Integer> spare = new Stack<>();

		for (int i = 3; i > 0; --i) {
			src.push(i);
		}
		
		System.out.format("Before => Source pole: %s, Destination pole: %s, Spare pole: %s\n", src, dest, spare);
		towerOfHanoi2(src.size(), src, dest, spare);
		System.out.format("After => Source pole: %s, Destination pole: %s, Spare pole: %s\n", src, dest, spare);
	}
}
