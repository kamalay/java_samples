package me.jangulaslam.javasamples;

public class Concurrency {

	public static void runTests() {
		//RunnableExample();
		//ThreadExample();
		//MultipleThreads();
		BankAccountExample();
	}
	
	public static void RunnableExample() {
		Job job = new Job(10);
		
		Thread worker = new Thread(job);
		Logger.log("Strating worker thread...");
		worker.start();
		try {
			Logger.log("Waiting for worker thread to exit...");
			worker.join();
			Logger.log("Main thread completed its wait on worker thread to exit");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void ThreadExample() {
		class MyThread extends Thread {
			int total;
			
			MyThread(int total) {
				this.total = total;
			}
			public void run() {
				Logger.log("Worker thread has started");
				for (int i = 0; i < this.total; ++i) {
					Logger.log("  " + i);
					try {
						Thread.sleep((this.total/2) * 100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				System.out.println();
				Logger.log("Worker thread is exiting...");
			}
		}
		
		MyThread worker = new MyThread(5);
		Logger.log("Strating worker thread...");
		worker.start();
		try {
			Logger.log("Waiting for worker thread to exit...");
			worker.join();
			Logger.log("Main thread completed its wait on worker thread to exit");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void MultipleThreads() {
		Thread worker1 = new Thread(new Job(4));
		Thread worker2 = new Thread(new Job(8));
		Thread worker3 = new Thread(new Job(12));
		
		worker1.start();
		worker2.start();
		worker3.start();
		
		try {
			worker1.join();
			worker2.join();
			worker3.join();
			
			Logger.log("Main thread completed its wait on all worker threads to exit");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void BankAccountExample() {
		class Withdraw implements Runnable {
			private BankAccount account;
			
			public Withdraw(BankAccount account) {
				this.account = account;
			}
			
			@Override
			public void run() {
				while (account.balance() > 0) {
					try {
						account.withdraw(10);
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		class Deposit implements Runnable {
			private BankAccount account;
			
			public Deposit(BankAccount account) {
				this.account = account;
			}
			
			@Override
			public void run() {
				while (account.balance() > 0) {
					try {
						account.deposit(10);
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		BankAccount account = new BankAccount("Jack", 100);
		account.addJointName("Jill");
		
		Thread deposit = new Thread(new Deposit(account));
		Thread withdraw = new Thread(new Withdraw(account));
		
		deposit.start();
		withdraw.start();
		
		try {
			deposit.join();
			withdraw.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
