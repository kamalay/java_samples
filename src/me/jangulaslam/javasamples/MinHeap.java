package me.jangulaslam.javasamples;

public class MinHeap extends HeapBase {
	public void prettyPrint() {
		System.out.println("\nMin Heap (tree): ");
		super.prettyPrint();
		
	}
	
	public void insert(int data) {
		// add at last
		list.add(data);
		// maintain heap (up direction)
		heapifyUp(list.size() - 1);
	}
	private void heapifyUp(int childLoc) {
		int parentLoc = Math.abs((childLoc-1)/2);
		
		if (childLoc == 0 || list.get(childLoc) > list.get(parentLoc)) {
			// reached top or child bigger then parent
			return;
		}
		
		swap(childLoc, parentLoc);
		heapifyUp(parentLoc);
	}

	public int removeMin() {
		// swap first and last
		int last = list.size() - 1;
		swap(0, last);

		// remove last and return (later)
		int data = list.get(last);
		list.remove(last);
		
		// maintain heap (down direction)
		if (!list.isEmpty()) {
			heapifyDown(0);
		}

		return data;
	}
	private void heapifyDown(int parentLoc) {
		int leftChildLoc = (2 * parentLoc) + 1;
		int rightChildLoc = (2 * parentLoc) + 2;

		//prettyPrint();
		
		int parent = list.get(parentLoc);
		if (leftChildLoc < list.size() && rightChildLoc < list.size()) {
			// both left and right child exist, swap parent with the smallest child
			if (list.get(leftChildLoc) < list.get(rightChildLoc)) {
				swap(parentLoc, leftChildLoc);
				heapifyDown(leftChildLoc);
			} else {
				swap(parentLoc, rightChildLoc);
				heapifyDown(rightChildLoc);
			}
		} else if (leftChildLoc < list.size() && parent > list.get(leftChildLoc))  {
			swap(parentLoc, leftChildLoc);
			heapifyDown(leftChildLoc);
		} else if (rightChildLoc < list.size() && parent > list.get(rightChildLoc)) {
			swap(parentLoc, rightChildLoc);
			heapifyDown(rightChildLoc);
		}
		
		// else (base case for recursion)
	}
}
