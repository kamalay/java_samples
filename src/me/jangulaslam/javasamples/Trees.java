package me.jangulaslam.javasamples;

public class Trees {
	static public void runTests() {

		{
//							----------------50--------------
//					--------30-------				--------70--------
//				----20----			x			----60---		----90----
//				10		15		x		x		x		x		80		x
			
			BinaryTree bt = new BinaryTree();
			// level = 0
			bt.root = new BTNode(50);
			
			// level = 1
			bt.root.left = new BTNode(30);
			bt.root.right = new BTNode(70);
			
			//  level = 2
			bt.root.left.left = new BTNode(20);
			bt.root.right.left = new BTNode(60);
			bt.root.right.right = new BTNode(90);
			
			// level = 3
			bt.root.left.left.left = new BTNode(10);
			bt.root.left.left.right = new BTNode(15);
			bt.root.right.right.left = new BTNode(80);

			System.out.println("\nTree height: " + bt.height());
			bt.prettyPrint();
		}
		
		{
			// binary trees
			BinaryTree bt = new BinaryTree();
			bt.root = new BTNode(20);
			bt.root.left = new BTNode(15);
			bt.root.right = new BTNode(30);
			
			bt.root.left.left = new BTNode(10);
			bt.root.left.right = new BTNode(50);
			bt.root.left.right.left = new BTNode(60);
			bt.root.left.right.left.right = new BTNode(100);
			
			bt.root.right.left = new BTNode(5);
			bt.root.right.right = new BTNode(15);
	
			bt.prettyPrint();
			bt.preOrderTraversalRecursive();
			bt.preOrderTraversalIterative();
			
			bt.inOrderTraversalRecursive();
			bt.inOrderTraversalIterative();
			
			bt.postOrderTraversalRecursive();
			bt.postOrderTraversalIterative();
			
			bt.levelOrderTraversal();
			
			System.out.println("\nTree height: " + bt.height());
			System.out.println("Is Binary Search Tree: " + bt.isBinarySearchTree());
			System.out.println("Is Balanced Binary Tree: " + bt.isBalancedBinaryTree());
			System.out.println("Is Balanced Binary Tree 2: " + bt.isBalancedBinaryTree2());
		}
	
		{
			{
				// binary search tree
				BinarySearchTree bst = new BinarySearchTree();
				
				int[] array = {30, 20, 40, 50, 60, 10, 35, 25, 45};
				for (int data : array) {
					bst.insert(data);
				}
				
				bst.prettyPrint();
				bst.preOrderTraversalRecursive();
				bst.inOrderTraversalRecursive();
				bst.postOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
				System.out.println("Is Binary Search Tree: " + bst.isBinarySearchTree());
				//System.out.println("Is Complete Binary Tree: " + bst.isCompleteBinaryTree());
				System.out.println("Is Balanced Binary Tree O(n2): " + bst.isBalancedBinaryTree());
				System.out.println("Is Balanced Binary Tree O (n): " + bst.isBalancedBinaryTree2());
			}
			
			{
				int[] sortedArray = {10};
				BinarySearchTree bst = BinarySearchTree.createMinimalBSTFromSortedArray(sortedArray);
				bst.prettyPrint();
				bst.inOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
				System.out.println("Is Binary Search Tree: " + bst.isBinarySearchTree());
			}
			
			{
				int[] sortedArray = {10, 20};
				BinarySearchTree bst = BinarySearchTree.createMinimalBSTFromSortedArray(sortedArray);
				bst.prettyPrint();
				bst.inOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
				System.out.println("Is Binary Search Tree: " + bst.isBinarySearchTree());
			}

			{
				int[] sortedArray = {10, 20, 30};
				BinarySearchTree bst = BinarySearchTree.createMinimalBSTFromSortedArray(sortedArray);
				bst.prettyPrint();
				bst.inOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
				System.out.println("Is Binary Search Tree: " + bst.isBinarySearchTree());
			}
			
			{
				int[] sortedArray = {10, 20, 30, 40, 50};
				BinarySearchTree bst = BinarySearchTree.createMinimalBSTFromSortedArray(sortedArray);
				bst.prettyPrint();
				bst.inOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
				System.out.println("Is Binary Search Tree: " + bst.isBinarySearchTree());
			}

			{
				int[] sortedArray = {10, 20, 30, 40, 50, 60, 70, 80, 90};
				BinarySearchTree bst = BinarySearchTree.createMinimalBSTFromSortedArray(sortedArray);
				bst.prettyPrint();
				bst.inOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
				System.out.println("Is Binary Search Tree: " + bst.isBinarySearchTree());
				System.out.println("Is Balanced Binary Tree O (n): " + bst.isBalancedBinaryTree2());
				{
					BTNode node = bst.findNode(60);
					if (node != null) {
						System.out.println("Found node: " + bst.findNodeRecursive(60).data);
					}
				}

				{
					BTNode node = bst.findNode(200);
					if (node == null) {
						System.out.println(200 + " node not found in BST");
					}
				}
				
			}

			{
				BinarySearchTree bst = new BinarySearchTree();
				bst.prettyPrint();
				System.out.println("\nTree height: " + bst.height());
			}

			{
				BinarySearchTree bst = new BinarySearchTree();
				bst.insert(30);
				bst.prettyPrint();
				bst.inOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
			}
			
			{
				BinarySearchTree bst = new BinarySearchTree();
				bst.insert(30);
				bst.insert(40);
				bst.prettyPrint();
				bst.inOrderTraversalRecursive();
				System.out.println("\nTree height: " + bst.height());
			}
		}
	}
}
