package me.jangulaslam.javasamples;

public class PriorityQueues {
	public static void runTests() {
		{
			MinHeap heap = new MinHeap();
			int[] array = {5, 3, 4, 2, 1, 6, 8, 9, 7, 0};//Arrays.getRandomUniqueArray(5);
			for (int data : array) {
				heap.insert(data);
			}
			heap.print();
			heap.prettyPrint();
			
			for (int i = 0; i < array.length; ++i) {
				int data = heap.removeMin();
				System.out.println("\nRemoved minumim: " + data);
				heap.print();
				//heap.prettyPrint();
			}
		}
	
		{
			MinHeap heap = new MinHeap();
			int[] array = Arrays.getSequenceArray(10);
			for (int data : array) {
				heap.insert(data);
			}
			heap.prettyPrint();
		}

		{
			MinHeap heap = new MinHeap();
			int[] array = Arrays.getRandomUniqueArray(5);
			for (int data : array) {
				heap.insert(data);
			}
			heap.prettyPrint();
		}

		{
			MinHeap heap = new MinHeap();
			int[] array = Arrays.getRandomUniqueArray(10);
			for (int data : array) {
				heap.insert(data);
			}
			heap.prettyPrint();
		}
	
		
		// max heaps
		{
			MaxHeap heap = new MaxHeap();
			int[] array = {5, 3, 4, 2, 1, 6, 8, 9, 7, 0};//Arrays.getRandomUniqueArray(5);
			for (int data : array) {
				heap.insert(data);
			}
			heap.print();
			heap.prettyPrint();
			
			for (int i = 0; i < array.length; ++i) {
				int data = heap.removeMax();
				System.out.println("\nRemoved maximum: " + data);
				heap.print();
				//heap.prettyPrint();
			}
		}
		
		{
			MaxHeap heap = new MaxHeap();
			int[] array = Arrays.getRandomUniqueArray(10);
			for (int data : array) {
				heap.insert(data);
			}
			heap.prettyPrint();
		}
	}
}
