/**
 * 
 */
package me.jangulaslam.javasamples;

import java.util.Stack;

/**
 * @author Jangul
 *
 */
public class StacksAndQueues {

	static public void runTests() throws Exception {
		// evaluate PRN expression
		{
			String expression = "3,4,*,1,2,+,+";
			System.out.println("PRN: " + expression + " => " + StacksAndQueues.evaluateRPN(expression));
		}
		{
			String expression = "1,1,+,-2,*";
			System.out.println("PRN: " + expression + " => " + StacksAndQueues.evaluateRPN(expression));
		}
	}
	
	static public int evaluateRPN(String expression) throws Exception {
		// RPN => Reverse Polish Notation, like 3,4,*,1,2,+,+
		
		Stack<Number> st = new Stack<Number>();
		
		String[] tokens = expression.split(",");
		
		for (String token : tokens) {
			try {
				int n = Integer.parseInt(token);
				st.push(n);
			} catch (NumberFormatException nfe) {
				int b = (int) st.pop();
				int a = (int) st.pop();
				switch (token) {
					case "+":
						st.push(a + b);
						break;
						
					case "-":
						st.push(a - b);
						break;
						
					case "*":
						st.push(a * b);
						break;
						
					case "/":
						st.push(a / b);
						break;
				
					default:
						throw new Exception("Don't know how to handle it" + token + " yet");
				}
			}
		}
		
		if (st.size() == 1) {
			return (int) st.pop();
		}
		
		throw new Exception("Don't know how to handle it" + expression + " yet");
	}
}
