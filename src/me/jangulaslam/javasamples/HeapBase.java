package me.jangulaslam.javasamples;

import java.util.ArrayList;

public class HeapBase {
	protected ArrayList<Integer> list = new ArrayList<>();
	
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	public void print() {
		System.out.println("\nMax Heap (array): " + list);
	}
	
	protected void swap(int loc1, int loc2) {
		int temp = list.get(loc1);
		list.set(loc1, list.get(loc2));
		list.set(loc2, temp);
	}
	
	public void prettyPrint() {
		if (list.isEmpty()) {
			System.out.println("<<empty-heap>>");
			return;
		}
		
		int level = 0;
		int elementsProcessed = 0;
		int maxElementsInALevel = 0;
		
		final int SPACE = 8;
		int nodePrintLocation = 0;
		int height = (int)(Math.log(list.size()) / Math.log(2));

		for (int i = 0; i < list.size();) {
			maxElementsInALevel = (int)Math.pow(2, level);
			nodePrintLocation = ((int) Math.pow(2, height - level)) * SPACE;
			//System.out.println("level: " + level + ", element: " + list.get(i));
			System.out.print(getPrintLine(i, nodePrintLocation));
			if (++i >= elementsProcessed + maxElementsInALevel) {
				level++;
				elementsProcessed += maxElementsInALevel;
				System.out.println();
			}
		}
		
		System.out.println();
	}
	
	private String getPrintLine(int parentLoc, int spaces) {
		StringBuilder sb = new StringBuilder();
		
		int i = 0;
		int to = spaces/2;

		int leftChildLoc = (2 * parentLoc) + 1;
		int rightChildLoc = (2 * parentLoc) + 2;
		
		for (; i < to; i++) {
			sb.append(' ');
		}
		to += spaces/2;
		char ch = ' ';
		if (leftChildLoc < list.size()) {
			ch = '_';
		}
		for (; i < to; i++) {
			sb.append(ch);
		}
		
		String value = Integer.toString(list.get(parentLoc));
		sb.append(value);
		
		to += spaces/2;
		ch = ' ';
		if (rightChildLoc < list.size()) {
			ch = '_';
		}
		for (i += value.length(); i < to; i++) {
			sb.append(ch);
		}
		
		to += spaces/2;
		for (; i < to; i++) {
			sb.append(' ');
		}
		
		return sb.toString();
	}
}
