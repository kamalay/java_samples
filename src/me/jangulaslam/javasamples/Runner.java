/**
 * 
 */
package me.jangulaslam.javasamples;



/**
 * @author Jangul
 *
 */
public class Runner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			//StacksAndQueues.runTests();
			//int[] array1 = Utils.getRandomArray(50);
			//Utils.printArray(array1);
			
			//SortingAlgorithms.runTests();
			
			//Fibonacci.runTests();
			//Factorial.runTests();
			
			//LinkedList.runTests();
			
			//DynamicProgramming.runTests();
			
			//Matrices.runTests();
			
			//Trees.runTests();
			//Graph.runTests();
			
			//PriorityQueues.runTests();
			
			//Concurrency.runTests();
			
			Strings.runTests();
			//Arrays.runTests();
			
			//Others.runTests();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
