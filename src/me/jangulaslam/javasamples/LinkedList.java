package me.jangulaslam.javasamples;

import java.util.Stack;

public class LinkedList {
	public class LLNode  {
		public int data;
		public LLNode next = null;
		
		public LLNode(int data) {
			this.data = data;
		}
	}
	
	public LLNode head = null;
	
	// construction
	public LinkedList() {
	}
	public LinkedList(int data) {
		this.head = new LLNode(data);
	}
	
	// insertion
	public void insert(int data) {
		if (head == null) {
			head = new LLNode(data);
			return;
		}
		
		LLNode node = head;
		while (node.next != null) {
			node = node.next;
		}
		
		node.next = new LLNode(data);
	}

	// recursive print (forward)
	public static void forwardPrintRecursive(LLNode node) {
		if (node != null) {
			System.out.format("% 4d  ", node.data);
			forwardPrintRecursive(node.next);
		}
	}
	public void forwardPrintRecursive() {
		System.out.println("\nForward print (recursive): ");
		forwardPrintRecursive(head);
	}

	// iterative print (forward)
	public static void forwardPrintIterative(LLNode node) {
		while (node != null) {
			System.out.format("% 4d  ", node.data);
			node = node.next;
		}
	}
	public void forwardPrintIterative() {
		System.out.println("\nForward print (iterative): ");
		forwardPrintIterative(head);
	}

	// recursive print (reverse)
	public static void reversePrintRecursive(LLNode node) {
		if (node != null) {
			reversePrintRecursive(node.next);
			System.out.format("% 4d  ", node.data);
		}
	}
	public void reversePrintRecursive() {
		System.out.println("\nReverse print (recursive): ");
		reversePrintRecursive(head);
	}

	// iterative print (reverse)
	public static void reversePrintItrative(LLNode node) {
		Stack<LLNode> st = new Stack<>();
		
		// put nodes in stack
		while (node != null) {
			st.push(node);
			node = node.next;
		}
		
		// pop nodes and print
		while(!st.isEmpty()) {
			node = st.pop();
			System.out.format("% 4d  ", node.data);
		}
	}
	public void reversePrintItrative() {
		System.out.println("\nReverse print (iterative): ");
		reversePrintItrative(head);
	}

	// reverse using recursion
	public void reverseRecursive(LLNode node)  {
		if (node == null) {
			return;
		} else if (node.next == null) {
			head = node;
		} else {
			// keep pushing
			reverseRecursive(node.next);
		
			// when popping out on the way back
			node.next.next = node;
			node.next = null;
		}
	}
	public void reverseRecursive()  {
		System.out.println("\nBefore reverse (using recursion): ");
		forwardPrintRecursive();
		reverseRecursive(head);
		System.out.println("\nAfter reverse (using recursion): ");
		forwardPrintRecursive();
	}

	// reverse using iteration
	public void reverseIterative(LLNode node)  {
		if (node == null) {
			return;
		} else if (node.next == null) {
			head = node;
		} else {
			LLNode prev = null;
			LLNode next;
			
			while (node != null) {
				next = node.next;
				node.next = prev;
				
				prev = node;
				node = next;
			}
			
			head = prev;
		}
	}
	public void reverseIterative()  {
		System.out.println("\nBefore reverse (using iterations): ");
		forwardPrintRecursive();
		reverseIterative(head);
		System.out.println("\nAfter reverse (using iterations): ");
		forwardPrintRecursive();
	}

	
	// tests
	public static void runTests() {
		{
			LinkedList list = new LinkedList(10);
			
			list.insert(20);
			list.insert(30);
			list.insert(40);
			list.insert(50);

			list.forwardPrintRecursive();
			list.forwardPrintIterative();
			list.reversePrintRecursive();
			list.reversePrintItrative();
			
			list.reverseRecursive();
			list.reverseIterative();
		}
	}


}
