package me.jangulaslam.javasamples;

public class Logger {
	public static void log(String message) {
		StringBuilder sb = new StringBuilder();
		sb.append("$$$ tid(");
		long threadID = Thread.currentThread().getId();
		sb.append(threadID);
		sb.append(") => ");
				
		System.out.println(sb + message);
	}
}
