package me.jangulaslam.javasamples;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree {

	public BTNode root = null;
	
	private static void preOrderTraversalRecursive(BTNode node) {
		if (node == null) {
			return;
		}
		
		System.out.format("% 4d ", node.data);
		preOrderTraversalRecursive(node.left);
		preOrderTraversalRecursive(node.right);
	}
	
	public void preOrderTraversalRecursive() {
		System.out.print("\nPre order traversal recursive:");
		preOrderTraversalRecursive(root);
	}

	
	// print a node, push the right child and move to left child, when leaf node, pop a node from stack
	public static void preOrderTraversalIterative(BTNode node) {
		Stack<BTNode> st = new Stack<BTNode>();
		while(node != null || !st.isEmpty()) {
			if (node != null) {
				System.out.format("% 4d ", node.data);
				st.push(node.right);
				node = node.left;
			} else {
				node = st.pop();
			}
		}
	}
	
	public void preOrderTraversalIterative() {
		System.out.print("\nPre order traversal iterative:");
		preOrderTraversalRecursive(root);
	}	
	
	private static void inOrderTraversalRecursive(BTNode node) {
		if (node == null) {
			return;
		}
		
		inOrderTraversalRecursive(node.left);
		System.out.format("% 4d ", node.data);
		inOrderTraversalRecursive(node.right);
	}

	public void inOrderTraversalRecursive() {
		System.out.print("\nIn order traversal recursive:");
		inOrderTraversalRecursive(root);
	}

	// push a node, keep moving to left, upon no left, pop node, print it and move to right
	public static void inOrderTraversalIterative(BTNode node) {
		
		Stack<BTNode> st = new Stack<BTNode>();
		while(node != null || !st.isEmpty()) {
			if (node != null) {
				st.push(node);
				// keep moving left
				node = node.left;
			} else {
				node = st.pop();
				System.out.format("% 4d ", node.data);
				node = node.right;
			}
		}
	}
	
	public void inOrderTraversalIterative() {
		System.out.print("\nIn order traversal iterative:");
		inOrderTraversalRecursive(root);
	}

	
	private static void postOrderTraversalRecursive(BTNode node) {
		if (node == null) {
			return;
		}
		
		postOrderTraversalRecursive(node.left);
		postOrderTraversalRecursive(node.right);
		System.out.format("% 4d ", node.data);
	}
	
	public void postOrderTraversalRecursive() {
		System.out.print("\nPost order traversal recursive:");
		postOrderTraversalRecursive(root);
	}

	public static void postOrderTraversalIterative(BTNode node) {
		Stack<BTNode> st = new Stack<BTNode>();
		BTNode lastVisited = null;
		
		while (node != null || !st.isEmpty()) {
			if (node != null) {
				st.push(node);
				node = node.left;
			} else {
				// peek the top now
				BTNode peek = st.peek();
				// if peek has right and it is not not yet visited
				if (peek.right != null && peek.right != lastVisited) {
					node = peek.right;
				} else {
					// visit and remember it as last visited
					System.out.format("% 4d ", peek.data);
					lastVisited = st.pop();
				}
			}
		}
	}	

	public void postOrderTraversalIterative() {
		System.out.print("\nPost order traversal iterative:");
		postOrderTraversalIterative(root);
	}

	
	public static void levelOrderTraversal(BTNode node) {
		Queue<BTNode> queue = new LinkedList<BTNode>();

		queue.add(node);
		while(! queue.isEmpty()) {
			node = queue.remove();

			System.out.format("% 4d ", node.data);
			if (node.left != null) queue.add(node.left);
			if (node.right != null) queue.add(node.right);
		}
	}

	public void levelOrderTraversal() {
		System.out.print("\nLevel order traversal:");
		levelOrderTraversal(root);
	}

	// prettyPrint
	public static void prettyPrint(BTNode node) {
		if (node == null) {
			return;
		}
		
		// get height first
		int height = heightRecursive(node);
		
		// perform  level order traversal
		Queue<BTNode> queue = new LinkedList<BTNode>();

		int level = 0;
		final int SPACE = 6;
		int nodePrintLocation = 0;
		
		// special node for pushing when a node has no left or right child (assumption, say this node is a node with value Integer.MIN_VALUE)
		BTNode special = new BTNode(Integer.MIN_VALUE);
		
		queue.add(node);
		queue.add(null);	// end of level 0
		while(! queue.isEmpty()) {
			node = queue.remove();
			
			if (node == null) {
				if (!queue.isEmpty()) {
					queue.add(null);
				}
				
				// start of new level
				System.out.println();
				level++;
			} else {
				nodePrintLocation = ((int) Math.pow(2, height - level)) * SPACE;
				
				System.out.print(getPrintLine(node, nodePrintLocation));
				
				if (level < height) {
					// only go till last level
					queue.add((node.left != null) ? node.left : special);
					queue.add((node.right != null) ? node.right : special);
				}
			}
		}		
	}
	public void prettyPrint() {
		System.out.println("\nBinary tree (pretty print):");
		prettyPrint(root);
	}
	private static String getPrintLine(BTNode node, int spaces) {
		StringBuilder sb = new StringBuilder();
		
		if (node.data == Integer.MIN_VALUE) {
			// for child nodes, print spaces
			for (int i = 0; i < 2 * spaces; i++) {
				sb.append(" ");
			}
			
			return sb.toString();
		}

		int i = 0;
		int to = spaces/2;

		for (; i < to; i++) {
			sb.append(' ');
		}
		to += spaces/2;
		char ch = ' ';
		if (node.left != null) {
			ch = '_';
		}
		for (; i < to; i++) {
			sb.append(ch);
		}
		
		String value = Integer.toString(node.data);
		sb.append(value);
		
		to += spaces/2;
		ch = ' ';
		if (node.right != null) {
			ch = '_';
		}
		for (i += value.length(); i < to; i++) {
			sb.append(ch);
		}
		
		to += spaces/2;
		for (; i < to; i++) {
			sb.append(' ');
		}
		
		return sb.toString();
	}
	
	
	// O (n)
	public BTNode findNode(int data) {
		BTNode node = root;
		Queue<BTNode> queue = new LinkedList<BTNode>();
		queue.add(root);
		
		while(! queue.isEmpty()) {
			node = queue.remove();
			if (node.data == data) {
				return node;
			}
			
			if (node.left != null) queue.add(node.left);
			if (node.right != null) queue.add(node.right);
		}

		return null;
	}

	
	
	private static int heightRecursive(BTNode  node) {
		if (node == null) {
			// empty tree
			return -1;
		}
		
		if (node.left == null && node.right == null) {
			// leaf node
			return 0;
		}
		
		return 1 + Math.max(heightRecursive(node.left), heightRecursive(node.right));
	}
	
	public int height() {
		return heightRecursive(root);
	}
	
	
	private static boolean isBinarySearchTree(BTNode node, int min, int max) {
		if (node == null || (node.left == null && node.right == null)) {
			// empty tree or leave nodes
			return true;
		}
		
		if (!(node.data >= min) && (node.data < max)) {
			// node data out of min and max range
			return false;
		}
		
		return isBinarySearchTree(node.left, min, node.data) && isBinarySearchTree(node.right, node.data, max);
	}
	
	public boolean isBinarySearchTree() {
		return isBinarySearchTree(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	
	
	private boolean isBalancedBinaryTree(BTNode node) {
		if (node == null) {
			return true;
		}
		
		int leftSubTreeHeight = heightRecursive(node.left);
		int rightSubTreeHeight = heightRecursive(node.right);
		
		int heightDiff = Math.abs(leftSubTreeHeight - rightSubTreeHeight);
		if (heightDiff > 1) {
			return false;
		}
		
		return isBalancedBinaryTree(node.left) && isBalancedBinaryTree(node.right);
	}

	// O (n2)
	public boolean isBalancedBinaryTree() {
		return isBalancedBinaryTree(root);
	}
	
	
	private int checkHeight(BTNode node) {
		if (node == null) {
			return 0;
		}

		int leftHeight = checkHeight(node.left);
		if (leftHeight == -1) {
			return -1;
		}
		
		int rightHeight = checkHeight(node.right);
		if (rightHeight == -1) {
			return -1;
		}
		
		int heightDiff = Math.abs(leftHeight - rightHeight);
		if (heightDiff > 1) {
			return -1;
		} else {
			return Math.max(leftHeight,  rightHeight) + 1;
		}
	}
	
	// O (n)
	public boolean isBalancedBinaryTree2() {
		if (checkHeight(root) == -1) {
			return false;
		}
		
		return true;
	}
}
