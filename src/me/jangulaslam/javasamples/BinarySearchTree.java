package me.jangulaslam.javasamples;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree extends BinaryTree {

	public BinarySearchTree() {
		super();
	}
	
	private void insert(BTNode node, BTNode dataNode) {
		if(dataNode.data <= node.data) {
			if (node.left == null) {
				node.left = dataNode;
			} else {
				insert(node.left, dataNode);
			}
		} else {
			if (node.right == null) {
				node.right = dataNode;
			} else {
				insert(node.right, dataNode);
			}
		}
	}
	
	public void insert(int data) {
		BTNode dataNode = new BTNode(data);
		if (root == null) {
			root = dataNode;
		} else {
			insert(root, dataNode);
		}
	}

	public void insert(BTNode dataNode) {
		if (root == null) {
			root = dataNode;
		} else {
			insert(root, dataNode);
		}
	}
	
	
	
	// O (log n)
	private BTNode findNodeRecursive(BTNode node, int data) {
		if (node == null) {
			return null;
		}
		
		if (data == node.data) {
			return node;
		} else if (data < node.data) {
			return findNodeRecursive(node.left, data);
		} else {
			return findNodeRecursive(node.right, data);
		}
	}
	

	public BTNode findNodeRecursive(int data) {
		return findNodeRecursive(root, data);
	}
	
	// O (log n)
	public BTNode findNode(int data) {
		BTNode node = root;
		Queue<BTNode> queue = new LinkedList<BTNode>();
		queue.add(root);
		
		while(! queue.isEmpty()) {
			node = queue.remove();
			if (node == null) {
				break;
			}
			if (node.data == data) {
				return node;
			} else if (data < node.data) {
				queue.add(node.left);
			} else {
				queue.add(node.right);
			}
		}

		return null;
	}	
	
	private static BTNode createMinimalBST(BinarySearchTree bst, int[] array, int min, int max) {
		if (min > max) {
			return null;
		}
		
		int mid = (min + max) / 2;
		BTNode node = new BTNode(array[mid]);
		bst.insert(node);
		
		node.left = createMinimalBST(bst, array, min, mid - 1);
		node.right = createMinimalBST(bst, array, mid + 1, max);
		return node;
	}
	
	public static BinarySearchTree createMinimalBSTFromSortedArray(int[] array) {
		BinarySearchTree bst = new BinarySearchTree();
		if (array.length == 0) {
			return bst;
		}

		createMinimalBST(bst, array, 0, array.length - 1);
		
		return bst;
	}

}
